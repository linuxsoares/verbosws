package domain;

public class Conjugado {
	private int id_conjugacao;
	private String conjugacao;
	private String nomeTempoVerbal;
	private String nomeTipoVerbal;
	
	public int getId_conjugacao() {
		return id_conjugacao;
	}
	public void setId_conjugacao(int id_conjugacao) {
		this.id_conjugacao = id_conjugacao;
	}
	public String getConjugacao() {
		return conjugacao;
	}
	public void setConjugacao(String conjugacao) {
		this.conjugacao = conjugacao;
	}
	public String getNomeTempoVerbal() {
		return nomeTempoVerbal;
	}
	public void setNomeTempoVerbal(String nomeTempoVerbal) {
		this.nomeTempoVerbal = nomeTempoVerbal;
	}
	public String getNomeTipoVerbal() {
		return nomeTipoVerbal;
	}
	public void setNomeTipoVerbal(String nomeTipoVerbal) {
		this.nomeTipoVerbal = nomeTipoVerbal;
	}
}
