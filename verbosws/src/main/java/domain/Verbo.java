package domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@XmlRootElement
public class Verbo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8878702368621398244L;
	
	private Long id_verbos;
	private String nome;
	
	public Long getId_verbos() {
		return id_verbos;
	}
	public void setId_verbos(Long id_verbos) {
		this.id_verbos = id_verbos;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String toJson() {
		Gson gson = new GsonBuilder().create();
		return gson.toJson(this);
	}
	@Override
	public String toString() {
		return "Verbos [id_verbos=" + id_verbos + ", nome=" + nome + "]";
	}
}
