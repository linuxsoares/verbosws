package config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigProperties {

	public String getProp(String name) throws Exception {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = this.getClass().getClassLoader().getResourceAsStream("applications.properties");
			prop.load(input);
			return prop.getProperty(name);
		} catch (Exception e) {
			throw new Exception("Erro getProperties: " + e.getMessage());
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					throw new Exception("Erro getProperties: " + e.getMessage());
				}
			}
		}
	} 

}
