package log;

import java.util.Date;
import java.util.Map;

import ch.qos.logback.classic.spi.IThrowableProxy;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.classic.spi.StackTraceElementProxy;
import ch.qos.logback.core.AppenderBase;
import ch.qos.logback.core.status.ErrorStatus;

import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongoDBAppender extends AppenderBase<LoggingEvent> {
	
	private MongoClient _mongo = null; 
	private String _dbHost = "localhost";
	private int _dbPort = 27017;
	private int _exLength = 5;
	private String _dbName = "logging";
	private String _dbCollection = "log";
	private MongoCollection<DBObject> _collection;

	@Override
	public void start() {
		try {
			if (_mongo != null) {
				_mongo.close();
			}
			_mongo = new MongoClient(_dbHost, _dbPort);

			MongoDatabase db = _mongo.getDatabase(_dbName);
			_collection = db.getCollection(_dbCollection, DBObject.class);
		} catch (Exception e) {
			_mongo = new MongoClient(_dbHost, _dbPort);
			addStatus(new ErrorStatus("Failed to initialize MondoDB", this, e));
			return;
		}
		super.start();
	}

	public void setDbHost(String dbHost) {
		_dbHost = dbHost;
	}

	public void setDbName(String dbName) {
		_dbName = dbName;
	}

	public void setDbPort(int dbPort) {
		_dbPort = dbPort;
	}

	public void setExLength(int exLength) {
		this._exLength = exLength;
	}
	
	public void setDbCollection(String dbCollection) {
		this._dbCollection = dbCollection;
	}
	
	@Override
	public void stop() {
		_mongo.close();
		super.stop();
	}

	@Override
	protected void append(LoggingEvent e) {

		StringBuilder ex = new StringBuilder();
		
		fillStackTrace(ex, e.getThrowableProxy());
		
		BasicDBObjectBuilder objectBuilder = BasicDBObjectBuilder.start()
				.add("ts", new Date(e.getTimeStamp()))
				.add("thread", e.getThreadName())
				.add("logger", e.getLoggerName())
				.add("level", e.getLevel().toString())
				.add("msg", e.getFormattedMessage())
				.add("ex", ex.toString())
				;
		if (e.hasCallerData()) {
			StackTraceElement st = e.getCallerData()[0];
			String callerData = String.format("%s.%s:%d", st.getClassName(), st.getMethodName(),
					st.getLineNumber());
			objectBuilder.add("caller", callerData);
		}
		Map<String, String> mdc = e.getMDCPropertyMap();
		if (mdc != null && !mdc.isEmpty()) {
			objectBuilder.add("mdc", new BasicDBObject(mdc));
		}
		_collection.insertOne(objectBuilder.get());
	}

	private void fillStackTrace(StringBuilder sb, IThrowableProxy throwable) {
		if (throwable != null) {
			sb.append("Caused by " + throwable.getClassName() + " : " + throwable.getMessage() + "\n");

			int commonFrames = _exLength;
			StackTraceElementProxy[] array = throwable.getStackTraceElementProxyArray();

			if (commonFrames == 0) {
				commonFrames = array.length; 
			}

			for (int i = 0; i < commonFrames; i++) {
				StackTraceElementProxy element = array[i];
				sb.append("\t" + element.getSTEAsString() + "\n");
			}
			
			fillStackTrace(sb, throwable.getCause());
		}
	}
}
