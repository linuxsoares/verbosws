package service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import service.VerbosService;
import dao.VerbosDAO;
import domain.Conjugado;
import domain.Verbo;

@Stateless
@Named("verbosService")
public class VerbosServiceImpl implements VerbosService {
	static final Logger log = LoggerFactory.getLogger(VerbosServiceImpl.class);
	
	@Inject
	private VerbosDAO dao;

	@Override
	public List<Verbo> getVerbos() throws Exception {
		log.info("VerbosServiceImpl getVerbos.");
		return dao.getVerbos();
	}

	@Override
	public List<Verbo> getVerbosPorLetra(String letra) throws Exception {
		log.info("VerbosServiceImpl getVerbosPorLetra -> " + letra);
		return dao.getVerbosPorLetra(letra);
	}

	@Override
	public List<Conjugado> getVerboConjugado(String palavra) throws Exception {
		log.info("VerbosServiceImpl getVerboConjugado -> " + palavra);
		return dao.getVerboConjugado(palavra);
	}

}
