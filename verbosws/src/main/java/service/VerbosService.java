package service;

import java.util.List;

import domain.Conjugado;
import domain.Verbo;

public interface VerbosService {
	
	public List<Verbo> getVerbos() throws Exception;

	public List<Verbo> getVerbosPorLetra(String letra) throws Exception;

	public List<Conjugado> getVerboConjugado(String palavra) throws Exception;

}
