package dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import connection.DBCollectionFactory;
import dao.AbstractDAO;
import dao.VerbosDAO;
import domain.Conjugado;
import domain.Verbo;

public class VerbosDAOImpl extends AbstractDAO implements VerbosDAO {
	private DBCursor cursor;
	static final Logger log = LoggerFactory.getLogger(VerbosDAOImpl.class);

	@Override
	public List<Verbo> getVerbos() throws Exception {
		log.info("VerbosDAO getVerbos.");
		List<Verbo> list = new ArrayList<>();
		String coll = prop.getProp("collection-list");
		DBCollection collection = DBCollectionFactory.getCollection(coll);
		BasicDBObject allQuery = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		BasicDBObject order = new BasicDBObject();
		
		fields.put("nome", 1);
		fields.put("id_verbos", 1);
		order.put("nome", 1);
		
		try {
			cursor = collection.find(allQuery, fields).sort(order);
			
			while(cursor.hasNext()){
				Verbo verbo = new Verbo();
				BasicDBObject obj = (BasicDBObject) cursor.next();
				verbo.setId_verbos(obj.getLong("id_verbos"));
				verbo.setNome(obj.getString("nome"));
				list.add(verbo);
			}
		} catch (Exception e) {
			log.error("Erro no DAO: getVerbos - Exception" + e.getMessage());
			throw new Exception("Erro no DAO: getVerbos - Exception" + e.getMessage());
		} finally {
			cursor.close();
		}
		return list;
	}

	@Override
	public List<Verbo> getVerbosPorLetra(String letra) throws Exception {
		log.info("VerbosDAO getVerbosPorLetra: "+letra);
		List<Verbo> list = new ArrayList<>();
		String coll = prop.getProp("collection-list");
		DBCollection collection = DBCollectionFactory.getCollection(coll);
		BasicDBObject allQuery = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		BasicDBObject order = new BasicDBObject();
		
		fields.put("nome", 1);
		fields.put("id_verbos", 1);
		order.put("nome", 1);
		allQuery.put("nome", new BasicDBObject("$regex", "^" + letra));
		
		try {
			cursor = collection.find(allQuery, fields).sort(order);
			
			while(cursor.hasNext()){
				Verbo verbo = new Verbo();
				BasicDBObject obj = (BasicDBObject) cursor.next();
				verbo.setId_verbos(obj.getLong("id_verbos"));
				verbo.setNome(obj.getString("nome"));
				list.add(verbo);
			}
		} catch (Exception e) {
			log.error("Erro no DAO: getVerbosPorLetra - Parametro: " + letra + "- Exception" + e.getMessage());
			throw new Exception("Erro no DAO: getVerbosPorLetra - Parametro: " + letra + "- Exception" + e.getMessage());
		} finally {
			cursor.close();
		}
		return list;
	}

	@Override
	public List<Conjugado> getVerboConjugado(String palavra) throws Exception {
		log.info("VerbosDAO getVerboConjugado: "+palavra);
		List<Conjugado> list = new ArrayList<Conjugado>();
		String coll = prop.getProp("collection-conjugados");
		DBCollection collection = DBCollectionFactory.getCollection(coll);
		BasicDBObject allQuery = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		
		fields.put("id_conjugacao", 1);
		fields.put("conjugacao", 1);
		fields.put("nomeTempoVerbal", 1);
		fields.put("nomeTipoVerbal", 1);
		allQuery.put("id_verbos", getIdVerbo(palavra));
		
		try {
			cursor = collection.find(allQuery, fields);
			
			while(cursor.hasNext()){
				Conjugado conj = new Conjugado();
				BasicDBObject obj = (BasicDBObject) cursor.next();
				conj.setId_conjugacao(obj.getInt("id_conjugacao"));
				conj.setConjugacao(obj.getString("conjugacao"));
				conj.setNomeTempoVerbal(obj.getString("nomeTempoVerbal"));
				conj.setNomeTipoVerbal(obj.getString("nomeTipoVerbal"));
				list.add(conj);
			}
		} catch (Exception e) {
			log.error("Erro no DAO: getVerboConjugado - Parametro: " + palavra + " - Exception" + e.getMessage());
			throw new Exception("Erro no DAO: getVerboConjugado - Parametro: " + palavra + " - Exception" + e.getMessage());
		} finally {
			cursor.close();
		}
		return list;
	}
	
	private int getIdVerbo(String verbo) throws Exception{
		log.info("VerbosDAO getIdVerbo: "+verbo);
		int idVerbo = 0;
		String coll = prop.getProp("collection-list");
		DBCollection collection = DBCollectionFactory.getCollection(coll);
		BasicDBObject allQuery = new BasicDBObject();
		BasicDBObject fields = new BasicDBObject();
		
		fields.put("id_verbos", 1);
		allQuery.put("nome", verbo);
		
		try {
			cursor = collection.find(allQuery, fields);
			
			while(cursor.hasNext()){
				BasicDBObject obj = (BasicDBObject) cursor.next();
				idVerbo = (int) obj.getLong("id_verbos");
			}
		} catch (Exception e) {
			log.error("Erro no DAO: getIdVerbo - Parametro: " + verbo + " - Exception" + e.getMessage());
			throw new Exception("Erro no DAO: getIdVerbo Parametro: " + verbo + " Exception" + e.getMessage());
		} finally {
			cursor.close();
		}
		return idVerbo;
	}

}
