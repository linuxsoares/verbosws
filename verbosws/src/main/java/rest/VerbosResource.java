package rest;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import service.VerbosService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import config.ConfigProperties;

@Path("/verbos")
public class VerbosResource {

	@Inject
	private VerbosService verbosService;
	
	private Gson gson = new GsonBuilder().create();
	static final Logger log = LoggerFactory.getLogger(VerbosResource.class);
	
	@GET
	@Path("/{param}")
	public Response getMsg(@PathParam("param") String msg) throws Exception {
		ConfigProperties prop = new ConfigProperties();
		String output = "Jersey say : " + msg + " Collection: " + prop.getProp("collection-list");
		return Response.status(200).entity(output).build();
	}
	
	@GET
	@Path("/lista")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getVerbos() throws Exception{
		log.info("Resource getVerbos");
		return Response.status(200).entity(gson.toJson(verbosService.getVerbos())).build();
	}
	
	@GET
	@Path("/letra/{param}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getVerbosPorLetra(@PathParam("param")String letra) throws Exception{
		log.info("Resource getVerbosPorLetra: " + letra);
		return Response.status(200).entity(gson.toJson(verbosService.getVerbosPorLetra(letra))).build(); 
				
	}
	
	@GET
	@Path("/palavra/{param}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getVerboConjugado(@PathParam("param")String palavra) throws Exception{
		log.info("Resource getVerboConjugado: " + palavra);
		return Response.status(200).entity(gson.toJson(verbosService.getVerboConjugado(palavra))).build();
	}

}
