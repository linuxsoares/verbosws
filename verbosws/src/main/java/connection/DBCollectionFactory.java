package connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;

import config.ConfigProperties;
import dao.AbstractDAO;


@SuppressWarnings("deprecation")
public class DBCollectionFactory extends AbstractDAO{
	
	private static DB db;
	static final Logger log = LoggerFactory.getLogger(DBCollectionFactory.class);
	
	static{
		Mongo mongo;
		try {
			ConfigProperties prop = new ConfigProperties();
			//log.info("Create connection MongoDB. URL: "+prop.getProp("location-mongodb") + " Port: "+prop.getProp("port-mongodb"));
			mongo = new MongoClient(prop.getProp("location-mongodb"), Integer.parseInt(prop.getProp("port-mongodb")));
			db = mongo.getDB(prop.getProp("database"));
			prop = null;
		} catch (Exception e) {
			log.error("Erro ao criar conex�o com MongoDB");
		}
		
		
	}
	
	public static DBCollection getCollection(String coll) throws Exception{
		try {
			return db.getCollection(coll);
		} catch (Exception e) {
			log.error("Erro ao retornar conex�o na Collection " + coll + "Error: " + e.getMessage());
			throw new Exception("Erro ao retornar conex�o na Collection " + coll + "Error: " + e.getMessage());
		}
	}

}
